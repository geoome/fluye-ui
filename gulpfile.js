'use strict';

var gulp = require('gulp'),
	sass = require('gulp-sass'),
	sassLint = require('gulp-sass-lint'),
	minifyCSS = require('gulp-minify-css'),
	autoprefixer = require('gulp-autoprefixer'),
	uglify = require('gulp-uglify'),

	rename = require('gulp-rename'),
	connect = require('gulp-connect'),
	open = require('gulp-open'),
	concat = require('gulp-concat'),
	port = process.env.port || 3031;


// scss compiler to css
gulp.task('scss', function () {
	gulp.src('./scss/*.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest('./css'));
});


// Lint for scss files
gulp.task('scss-lint', function () {
	gulp.src('./scss/*.scss')
		.pipe(sassLint())
		.pipe(sassLint.format())
		.pipe(sassLint.failOnError());
});


// css processing
gulp.task('css', function () {
	gulp.src('./css/fly-app.css')
		.pipe(minifyCSS())
		.pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9'))
		.pipe(concat('fly-app.min.css'))
		.pipe(gulp.dest('./dist/css'));

	gulp.src('./css/fly-mkt.css')
		.pipe(minifyCSS())
		.pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9'))
		.pipe(concat('fly-mkt.min.css'))
		.pipe(gulp.dest('./dist/css'));

	gulp.src('./css/fly-auth.css')
		.pipe(minifyCSS())
		.pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9'))
		.pipe(concat('fly-auth.min.css'))
		.pipe(gulp.dest('./dist/css'));
});


// js processing
gulp.task('js', function () {

	var source = [
		//'./vendor/bootstrap-sass/assets/javascripts/bootstrap/affix.js',
		'./vendor/bootstrap-sass/assets/javascripts/bootstrap/alert.js',
		'./vendor/bootstrap-sass/assets/javascripts/bootstrap/button.js',
		'./vendor/bootstrap-sass/assets/javascripts/bootstrap/carousel.js',
		'./vendor/bootstrap-sass/assets/javascripts/bootstrap/collapse.js',
		'./vendor/bootstrap-sass/assets/javascripts/bootstrap/dropdown.js',
		'./vendor/bootstrap-sass/assets/javascripts/bootstrap/modal.js',
		//'./vendor/bootstrap-sass/assets/javascripts/bootstrap/popover.js',
		//'./vendor/bootstrap-sass/assets/javascripts/bootstrap/scrollspy.js',
		//'./vendor/bootstrap-sass/assets/javascripts/bootstrap/tab.js',
		//'./vendor/bootstrap-sass/assets/javascripts/bootstrap/tooltip.js',
		'./vendor/bootstrap-sass/assets/javascripts/bootstrap/transition.js',
		'./vendor/PACE/pace.js',
		'./javascripts/fluye-core.js'];

	gulp.src(source)
		.pipe(concat('fly-core.js'))
		.pipe(gulp.dest('./dist/js'))
		.pipe(uglify())
		.pipe(rename('fly-core.min.js'))
		.pipe(gulp.dest('./dist/js'));

	gulp.src(['./javascripts/fluye-app.js'])
		.pipe(concat('fly-app.js'))
		.pipe(gulp.dest('./dist/js'))
		.pipe(uglify())
		.pipe(rename('fly-app.min.js'))
		.pipe(gulp.dest('./dist/js'));
});


// fonts
gulp.task('fonts', function () {
	gulp.src('./fonts/bootstrap/*')
		.pipe(gulp.dest('dist/fonts/bootstrap'));
});


// img
gulp.task('images', function () {
	gulp.src('./img/*')
		.pipe(gulp.dest('./docs/assets/img'));
});


// watch files for live reload
gulp.task('watch', function () {
	gulp.watch('./scss/**/*.scss', ['scss', 'css to docs']);
	gulp.watch('./javascripts/*.js', ['js', 'js to docs']);
});


// copying css to docs site
gulp.task('css to docs', function () {
	gulp.src('./css/*')
		.pipe(gulp.dest('./docs/assets/css'));
});

// copying js to docs site
gulp.task('js to docs', function () {

	var source = [
		//'./vendor/bootstrap-sass/assets/javascripts/bootstrap/affix.js',
		'./vendor/bootstrap-sass/assets/javascripts/bootstrap/alert.js',
		'./vendor/bootstrap-sass/assets/javascripts/bootstrap/button.js',
		'./vendor/bootstrap-sass/assets/javascripts/bootstrap/carousel.js',
		'./vendor/bootstrap-sass/assets/javascripts/bootstrap/collapse.js',
		'./vendor/bootstrap-sass/assets/javascripts/bootstrap/dropdown.js',
		'./vendor/bootstrap-sass/assets/javascripts/bootstrap/modal.js',
		//'./vendor/bootstrap-sass/assets/javascripts/bootstrap/popover.js',
		//'./vendor/bootstrap-sass/assets/javascripts/bootstrap/scrollspy.js',
		//'./vendor/bootstrap-sass/assets/javascripts/bootstrap/tab.js',
		//'./vendor/bootstrap-sass/assets/javascripts/bootstrap/tooltip.js',
		'./vendor/bootstrap-sass/assets/javascripts/bootstrap/transition.js',
		'./vendor/PACE/pace.js',
		'./javascripts/fluye-core.js'];

	gulp.src(source)
		.pipe(gulp.dest('./docs/assets/js'));
});

// copying fonts to docs site
gulp.task('fonts to docs', function () {
	gulp.src(['./fonts/bootstrap/*'])
		.pipe(gulp.dest('./docs/assets/fonts/bootstrap'));
});

// copying fonts to docs site
gulp.task('img to docs', function () {
	gulp.src(['./img/*'])
		.pipe(gulp.dest('./docs/assets/img'));
});


// Gulp named tasks
gulp.task('default', ['compile', 'docs', 'watch']);

gulp.task('compile', ['scss', 'js', 'images', 'fonts']);
gulp.task('docs', ['css to docs', 'js to docs', 'fonts to docs', 'img to docs']);
gulp.task('build', ['compile', 'css', 'js', 'fonts']);
gulp.task('lint', ['sass-lint']);
