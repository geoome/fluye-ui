/*! Fluye Core UI library
 * ================
 * Fluye Core library v1. This file should be included in all pages
 * It controls some layout options and implements exclusive FluyeUI plugins.
 *
 * @version 1.0.0
 */

//Make sure jQuery has been loaded before app.js
if (typeof jQuery === "undefined") {
	throw new Error("Fluye Core UI requires jQuery");
}


/* Fluye UI
 *
 * @type Object
 * @description $.Fluye is the main object for the template's app.
 *              It's used for implementing functions and options related
 *              to the template.
 */
$.FluyeUI = {};

/* --------------------
 * - Fluye Options -
 * --------------------
 * Modify these options to suit your implementation
 */
$.FluyeUI.options = {
	//Activate sidebar push menu
	sidebarPushMenu: true,
	//General animation speed for JS animated elements such as box collapse/expand and
	//sidebar treeview slide up/down. This options accepts an integer as milliseconds,
	//'fast', 'normal', or 'slow'
	animationSpeed: 500,
	//Sidebar push menu toggle button selector
	sidebarToggleSelector: "[data-toggle='offcanvas']",
	//Enable sidebar expand on hover effect for sidebar mini
	//This option is forced to true if both the fixed layout and sidebar mini
	//are used together
	sidebarExpandOnHover: false,
	//The standard screen sizes that bootstrap uses.
	//If you change these in the variables.less file, change
	//them here too.
	screenSizes: {
		xs: 480,
		sm: 768,
		md: 992,
		lg: 1200
	}
};


$(function () {
	"use strict";

	//Fix for IE page transitions
	$('body').removeClass('hold-transition');


	//Extend options if external options exist
	if (typeof FluyeUIOptions !== "undefined") {
		$.extend(true,
			$.FluyeUI.options,
			FluyeUIOptions);
	}

	//Easy access to options
	var o = $.FluyeUI.options;

	//Set up the object
	_init();

	//Activate the layout maker
	$.FluyeUI.layout.activate();

	//Enable sidebar tree view controls
	$.FluyeUI.tree('.sidebar');

	//Activate sidebar push menu
	if (o.sidebarPushMenu) {
		$.FluyeUI.pushMenu.activate(o.sidebarToggleSelector);
	}

	$('body').addClass('fixed');

});

/* ----------------------------------
 * - Initialize the Fluye UI Object -
 * ----------------------------------
 * All FluyeUI functions are implemented below.
 */
function _init() {
	'use strict';

	/* Layout
	 * ======
	 * Fixes the layout height in case min-height fails.
	 *
	 * @type Object
	 * @usage $.FluyeUI.layout.activate()
	 *        $.FluyeUI.layout.fix()
	 *        $.FluyeUI.layout.fixSidebar()
	 */
	$.FluyeUI.layout = {
		activate: function () {
			var _this = this;
			_this.fix();
			$(window, ".wrapper").resize(function () {
				_this.fix();
			});
		},
		fix: function () {
			//Get window height and the wrapper height
			var neg = $('.header').outerHeight() + $('.footer').outerHeight();
			var window_height = $(window).height();
			var sidebar_height = $(".sidebar").height();
			//Set the min-height of the content and sidebar based on the
			//the height of the document.
			var postSetWidth;

			if (window_height >= sidebar_height) {
				$(".content-wrapper, .right-side").css('min-height', window_height - neg);
				postSetWidth = window_height - neg;
			} else {
				$(".content-wrapper, .right-side").css('min-height', sidebar_height);
				postSetWidth = sidebar_height;
			}
		}
	};

	/* PushMenu()
	 * ==========
	 * Adds the push menu functionality to the sidebar.
	 *
	 * @type Function
	 * @usage: $.FluyeUI.pushMenu("[data-toggle='offcanvas']")
	 */
	$.FluyeUI.pushMenu = {
		activate: function (toggleBtn) {
			//Get the screen sizes
			var screenSizes = $.FluyeUI.options.screenSizes;

			//Enable sidebar toggle
			$(document).on('click', toggleBtn, function (e) {
				e.preventDefault();

				console.log('click on!');

				//Enable sidebar push menu
				if ($(window).width() > (screenSizes.sm - 1)) {
					if ($('body').hasClass('sidebar-collapse')) {
						$('body').removeClass('sidebar-collapse').trigger('expanded.pushMenu');
					} else {
						$('body').addClass('sidebar-collapse').trigger('collapsed.pushMenu');
					}
				}
				//Handle sidebar push menu for small screens
				else {
					if ($('body').hasClass('sidebar-open')) {
						$('body').removeClass('sidebar-open').removeClass('sidebar-collapse').trigger('collapsed.pushMenu');
					} else {
						$('body').addClass('sidebar-open').trigger('expanded.pushMenu');
					}
				}
			});

			$(".content-wrapper").click(function () {
				//Enable hide menu when clicking on the content-wrapper on small screens
				if ($(window).width() <= (screenSizes.sm - 1) && $("body").hasClass("sidebar-open")) {
					$("body").removeClass('sidebar-open');
				}
			});

			//Enable expand on hover for sidebar mini
			if ($.FluyeUI.options.sidebarExpandOnHover
				|| ($('body').hasClass('fixed')
				&& $('body').hasClass('sidebar-mini'))) {
				this.expandOnHover();
			}
		},
		expandOnHover: function () {
			var _this = this;
			var screenWidth = $.FluyeUI.options.screenSizes.sm - 1;
			//Expand sidebar on hover
			$('.sidebar').hover(function () {
				if ($('body').hasClass('sidebar-mini')
					&& $("body").hasClass('sidebar-collapse')
					&& $(window).width() > screenWidth) {
					_this.expand();
				}
			}, function () {
				if ($('body').hasClass('sidebar-mini')
					&& $('body').hasClass('sidebar-expanded-on-hover')
					&& $(window).width() > screenWidth) {
					_this.collapse();
				}
			});
		},
		expand: function () {
			$("body").removeClass('sidebar-collapse').addClass('sidebar-expanded-on-hover');
		},
		collapse: function () {
			if ($('body').hasClass('sidebar-expanded-on-hover')) {
				$('body').removeClass('sidebar-expanded-on-hover').addClass('sidebar-collapse');
			}
		}
	};


	/* Tree()
	 * ======
	 * Converts the sidebar into a multilevel
	 * tree view menu.
	 *
	 * @type Function
	 * @Usage: $.AdminLTE.tree('.sidebar')
	 */
	$.FluyeUI.tree = function (menu) {
		var _this = this;
		var animationSpeed = $.FluyeUI.options.animationSpeed;
		$(document).off('click', menu + ' li a')
			.on('click', menu + ' li a', function (e) {
				//Get the clicked link and the next element
				var $this = $(this);
				var checkElement = $this.next();

				//Check if the next element is a menu and is visible
				if ((checkElement.is('.treeview-menu')) && (checkElement.is(':visible')) && (!$('body').hasClass('sidebar-collapse'))) {
					//Close the menu
					checkElement.slideUp(animationSpeed, function () {
						checkElement.removeClass('menu-open');
						//Fix the layout in case the sidebar stretches over the height of the window
						//_this.layout.fix();
					});
					checkElement.parent("li").removeClass("active");
				}
				//If the menu is not visible
				else if ((checkElement.is('.treeview-menu')) && (!checkElement.is(':visible'))) {
					//Get the parent menu
					var parent = $this.parents('ul').first();
					//Close all open menus within the parent
					var ul = parent.find('ul:visible').slideUp(animationSpeed);
					//Remove the menu-open class from the parent
					ul.removeClass('menu-open');
					//Get the parent li
					var parent_li = $this.parent("li");

					//Open the target menu and add the menu-open class
					checkElement.slideDown(animationSpeed, function () {
						//Add the class active to the parent li
						checkElement.addClass('menu-open');
						parent.find('li.active').removeClass('active');
						parent_li.addClass('active');
						//Fix the layout in case the sidebar stretches over the height of the window
						_this.layout.fix();
					});
				}
				//if this isn't a link, prevent the page from being redirected
				if (checkElement.is('.treeview-menu')) {
					e.preventDefault();
				}
			});
	};


}



