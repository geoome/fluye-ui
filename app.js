var express = require('express');
var app = express();

app.get('/', function (req, res) {
	var timeout = 5000;
	setTimeout(function(){ res.send('Foo response'); }, timeout);
});

app.listen(3000, function () {
	console.log('App is listening on port 3000!');
});
